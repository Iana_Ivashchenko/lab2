%include "colon.inc"

section .data
%include "words.inc"
err_msg: db 'Значения по такому ключу не найдено!', 10, 0
input_tooltip: db 'Введите ключ: ', 0
answer_tooltip: db 'Значение: ', 0


section .text
global _start
extern read_word
extern find_word
extern string_length
extern print_string
extern print_newline
extern exit
extern err_string

_start:
    mov rdi, input_tooltip
    call print_string

    xor rdi, rdi
    sub rsp, 255        ; буфер размером 255 (по условию)
    mov rdi, rsp        ; сохраняем в rdi
    mov rsi, 255        ; кол-во символов
    call read_word      ; считываем слово

    mov rsi, colon_last ; указатель на последний элемент в списке
    mov rdi, rax        ; в rdi введенное слово
    call find_word      ; находим слово; 0 - не нашлось
    add rsp, 255        ; возвращаем указатель стека наместо
    
    test rax, rax       ; если ключ не найден то rax = 0
    jz .notFound

    mov rdi, answer_tooltip
    call print_string

    mov rdi, rax
    call print_string
    call print_newline

    xor rdi, rdi
    call exit
    ret

.notFound:
    mov rdi, err_msg
    call err_string
    
    mov rdi, 1
    call exit


